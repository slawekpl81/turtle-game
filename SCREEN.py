import turtle

class Screen:
    def __init__(self, title):
        self.screen_obj = turtle.Screen()
        self.screen_obj.title(title)

    def start(self):
        self.screen_obj.title('My Turtle')
        self.screen_obj.bgcolor('blue')

    def start_listen(self, turtle_obj):
        self.screen_obj.listen()
        self.screen_obj.onkey(turtle_obj.up, "Up")
        self.screen_obj.onkey(turtle_obj.down, "Down")
        self.screen_obj.onkey(turtle_obj.left, "Left")
        self.screen_obj.onkey(turtle_obj.right, "Right")
        self.screen_obj.mainloop()