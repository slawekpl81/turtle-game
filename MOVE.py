import turtle

class Move:
    def __init__(self):
        self.turtle_obj = turtle.Turtle()
        self._step = 10
        self._angle = 90

    def up(self):
        self.turtle_obj.setheading(self._angle)
        self.turtle_obj.forward(self._step)

    def down(self):
        self.turtle_obj.setheading(self._angle + 180)
        self.turtle_obj.forward(self._step)

    def left(self):
        self.turtle_obj.setheading(self._angle + 90)
        self.turtle_obj.forward(self._step)

    def right(self):
        self.turtle_obj.setheading(0)
        self.turtle_obj.forward(self._step)
