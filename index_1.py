import turtle, time
from MOVE import Move
from SCREEN import Screen

def turtle_square(tu:turtle.Turtle, a:int):
    tu.rt(a)
    tu.fd(100)
    tu.rt(90)
    tu.fd(100)
    tu.rt(90)
    tu.fd(100)
    tu.rt(90)
    tu.fd(100)

if __name__ == '__main__':
    my_screen = Screen('Turtle GAME')
    my_player = Move()

    my_screen.start_listen(my_player)